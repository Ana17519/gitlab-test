FROM centos:7

RUN yum makecache \
    && yum install -y gcc openssl-devel bzip2-devel libffi-devel wget make

RUN wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz \
    && tar xzf Python-3.7.0.tgz \
    && cd Python-3.7.0 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && rm -rf ../Python-3.7.0.tgz ../Python-3.7.0

COPY python-api.py /opt/python_api/python-api.py

RUN pip3.7 install -U  --upgrade pip && pip3.7 install flask flask-jsonpify flask-restful

EXPOSE 5290

CMD ["python3.7", "/opt/python_api/python-api.py"]